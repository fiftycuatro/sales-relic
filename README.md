# salesrelic 

Sample clojure/clojurescript application.

## Getting Started (DEV)

Install [Leinigen](https://leiningen.org). Leinigen is the clojure build tool
used for this project

Next create a copy of `secrets.sample.edn` and name it `secrets.edn`. This file
will contain any sensitive configuration that should not be checked into source
control.

The application requires a database. To start the postgresql database instance
locally run the following docker command. This will use the standard docker
file.

```
$ docker run --name my-postgres -e POSTGRES_PASSWORD=password -p 5432:5432 -d postgres
```

Start the REPL

```
$ lein repl
```

Once the REPL Finally start the app using the `(go)` function.

```
user=> (go)
```

This will compile the clojurescript code and start the backend http server.

Navigate to [http://localhost:8082](http://localhost:8082). A user of `admin`
password `password` will be automatically created.

## Additional Dev Notes

Any frontend changes will automatically be hotloaded into the browser.

Backend changes can be made and the `(reset)` function can be entered into
REPL to stop, reload, recompile and restart the system. This can be used to
quickly iterate on the application. Usually if an error is encountered
during `(reset)` it is required to restart the REPL instance.

GraphiQL interface at [http://localhost:8082/graphiql](http://localhost:8082/graphiql)

## License

MIT License

Copyright © 2018 Phillip Gomez

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE
