(ns salesrelic.components.account-menu
  (:require
    [reagent.core :as reagent]
    [re-frame.core :refer [dispatch]]
    [salesrelic.components.material-ui :as mui]))

(defn MenuPopup
  [{:keys [open? user on-logout]}]
  (fn [props]
    (let [props (js->clj props :keywordize-keys true)
          transitionProps (js->clj (:TransitionProps props) :keywordize-keys true)
          placement (:placement props)]
      (reagent/as-element
        [mui/Grow
         (merge transitionProps
                {:style {:transformOrigin (if (= placement "bottom") "center top" "center-bottom")}})
         [mui/Paper
          [mui/ClickAwayListener {:onClickAway #(reset! open? false)}
           [mui/MenuList
            [mui/MenuItem {:onClick #(do
                                       (reset! open? false)
                                       (dispatch [:navigate-to :users/by-id :id (:id user)]))} "Profile"]
            [mui/MenuItem {:onClick on-logout} "Logout"]]]]]))))

(defn LoggedInUserMenu
  [{:keys [user on-logout] :as args}]
  (let [open? (reagent/atom false)
        anchorEl (reagent.core/atom nil)]
    (fn [{:keys [user] :as args}]
      [:div
       [mui/Button {:color "inherit"
                    :aria-label "Menu"
                    :buttonRef #(reset! anchorEl %)
                    :onClick #(reset! open? true)}
        [mui/Typography {:color "inherit"
                         :style {:margin-right 5}} (:name user)]
        [mui/Icon {:className "fal fa-user-circle"}]]

       [mui/Popper
        {:open @open?
         :anchorEl @anchorEl
         :transition true
         :disablePortal true}

        ;; Poppers are handled a little differently. They require a
        ;; fn that returns a react element. The fn will get a props
        ;; map.
        (MenuPopup (merge {:open? open?} args))]]
  )))


(defn AccountMenu
  [{:keys [user on-login on-logout] :as args}]
  (if (not (:authenticated user))
    [mui/Button {:color   "inherit"
                 :onClick on-login} "Login"]
    [LoggedInUserMenu args]

    ))
