(ns salesrelic.components.nav-menu
  (:require
    [salesrelic.css :as css]
    [salesrelic.components.material-ui :as mui]))

(def style
  {:toolbar (mui/theme :mixins :toolbar)})

(defn NavMenuItem
  [{:keys [label icon on-click]}]
  [mui/ListItem {:button true
                 :onClick on-click}
   [mui/ListItemIcon icon]
   [mui/ListItemText {:primary label}]])

(defn NavMenu
  [{:keys [title items]}]
  (let [classes (css/get-and-attach-classes style)]
    [:div
     [mui/Toolbar {:className (:toolbar classes)}
      [mui/Typography {:variant "subheading"} title]]
     [mui/Divider]
     [mui/List {:component "nav"}
      (for [item items]
        ^{:key item} [NavMenuItem item])
      ]]))