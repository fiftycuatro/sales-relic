(ns salesrelic.components.tables
  (:require [salesrelic.components.material-ui :as mui]))

(defn TableHeaderCell
  [{:keys [model]}]
  [mui/TableCell
   [mui/TableSortLabel (:label model)]])

(defn TextTableCell
  [{:keys [data model]}]
  [mui/TableCell {:component "th" :scope "row"} (get data (:key model))])

(defn TableHeader
  [{:keys [model classes]}]
  [mui/TableHead
   [mui/TableRow
    (for [item model]
      (let [template (or (:header-template item)
                         TableHeaderCell)
            key (symbol (str (:key item) "-header"))]
        ^{:key key} [template {:model item
                               :classes classes}]))]])

(defn TableBody
  [{:keys [data data-key-fn model classes]}]
  [mui/TableBody
   (for [row-item data]
     ^{:key (data-key-fn row-item)}
     [mui/TableRow
      (for [model-item model]
        (let [template (or (:body-template model-item)
                           TextTableCell)
              key (symbol (str (data-key-fn row-item) "-" (:key model-item)))]
          ^{:key key} [template {:data  row-item
                                 :model model-item
                                 :classes classes}]))])])

(defn Table
  [args]
  (let [defaults {:classes      {}
                  :data-key-fn :id}
        args (merge defaults args)]
    [mui/Table
     [TableHeader args]
     [TableBody   args]]))
