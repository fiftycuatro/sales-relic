(ns salesrelic.components.page-layout
  (:require
    [reagent.core :as reagent]
    [salesrelic.css :as css]
    [salesrelic.components.material-ui :as mui]))

(def style
  {:root          {:flex-grow 1
                   :z-index 1
                   :overflow "hidden"
                   :position "relative"
                   :display "flex"
                   :width "100%"}
   :appbar        {:position "absolute"
                   :margin-left "240px"
                   (mui/breakpoint :up :md) {:width "calc(100% - 240px)"}}
   :title         {:flex 1}
   :nav-icon-hide {(mui/breakpoint :up :md) {:display "none"}}
   :toolbar       (mui/theme :mixins :toolbar)
   :drawer-paper  {:width "240px"
                   (mui/breakpoint :up :md) {:position "relative"}}
   :content       {:flex-grow 1
                   :padding-top "80px"
                   :background-color (mui/theme :palette :background :default)
                   :padding (* 3 (mui/theme :spacing :unit))}})

(defn PageLayout
  [{:keys [content content-title nav-menu account-menu]}]
  (let [classes (css/get-and-attach-classes style)
        open-drawer (reagent/atom false)]
    (fn [{:keys [content content-title nav-menu account-menu]}]
      [:div {:className (:root classes)}
       [mui/AppBar {:className (:appbar classes)
                    :color "primary"}
        [mui/Toolbar
         [mui/IconButton {:color "inherit"
                          :aria-label "Menu"
                          :onClick #(swap! open-drawer not)
                          :className (:nav-icon-hide classes)}
          [mui/Icon {:className "fal fa-bars"}]]
         [mui/Typography {:className (:title classes)
                          :variant "title"
                          :color "inherit"} content-title]
         account-menu]]

       [mui/Hidden {:mdUp true}
        [mui/Drawer {:variant "temporary"
                     :anchor "left"
                     :open @open-drawer
                     :onClose #(swap! open-drawer not)
                     :classes {:paper (:drawer-paper classes)}
                     :ModalProps {:keepMounted true}}
         nav-menu]]

       [mui/Hidden {:smDown true
                    :implementation "css"}
        [mui/Drawer {:variant "permanent"
                     :open true
                     :classes {:paper (:drawer-paper classes)}}
         nav-menu]]
       [:main {:className (:content classes)}
        [:div {:className (:toolbar classes)}
         content]]])))
