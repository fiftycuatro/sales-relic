(ns salesrelic.components.material-ui
  (:refer-clojure :exclude [List])
  (:require-macros
    [salesrelic.components.macros :refer [export-material-ui-react-classes]])
  (:require
    [reagent.core :as reagent]
    [cljsjs.material-ui]))

;;----------- Theme -----------------------------------------------------------

(def palette
  {:primary   {:light        "#5efc82"
               :main         "#00c853"
               :dark         "#009624"
               :contrastText "#ffffff"}
   :secondary {:light        "#ecfffd"
               :main         "#ff5e00"
               :dark         "#88c399"
               :contrastText "#000000"}})


(def MuiThemeProvider (-> js/MaterialUIStyles
                          (aget "MuiThemeProvider")
                          reagent/adapt-react-class))

(defn create-theme-with-palette
  [palette]
  (js->clj
    (.createMuiTheme js/MaterialUIStyles (clj->js {:palette palette}))
    :keywordize-keys true))

(def mui-theme (create-theme-with-palette palette))

(defn theme
  ([] mui-theme)
  ([& keys] (get-in mui-theme keys)))

;;-------------- Create Material-UI Components --------------------------------

(export-material-ui-react-classes)


;;-------------- Helper Methods -----------------------------------------------

(defn breakpoint
  [mode point]
  ((theme :breakpoints mode) (name point)))

(defn gutters
  [style]
  ((theme :mixins :gutters) (clj->js style)))


