(ns salesrelic.components.macros
  (:require
    [reagent.core :as reagent]))

;; ----------- Notes ----------------------------------------------------------
;; This file must be declared as a clj.

(def material-tags
  '[
    AppBar
    Button
    CssBaseline
    Checkbox
    Chip
    CircularProgress
    ClickAwayListener
    Dialog
    DialogTitle
    DialogContent
    DialogContentText
    DialogActions
    Divider
    Drawer
    Fade
    FormControl
    Grow
    Hidden
    Icon
    IconButton
    Input
    InputLabel
    List
    ListItem
    ListItemIcon
    ListItemText
    Menu
    MenuItem
    MenuList
    Paper
    Popper
    ;;MenuIcon
    Select
    Slide
    Table
    TableHead
    TableBody
    TableRow
    TableCell
    TableSortLabel
    TextField
    Toolbar
    Typography])

(defn material-ui-react-import [tname]
  `(def ~tname
     (reagent/adapt-react-class (aget js/MaterialUI ~(name tname)))))

(defmacro export-material-ui-react-classes []
  `(do
     ~@(map material-ui-react-import material-tags)
     ))

