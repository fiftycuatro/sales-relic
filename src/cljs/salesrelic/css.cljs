(ns salesrelic.css
  (:require
    [cljsjs.material-ui]
    [goog.object]))

;; ------------------- Notes --------------------------------------------------
;; Ideally I would of liked to use the JSS that is being used by Material-UI
;; but I can't really figure out how to

(def ^:private jss-preset
  (aget js/MaterialUIStyles "jssPreset"))

(def ^:private jss
  (.create js/jss (jss-preset)))

(defn create-stylesheet
  [styles]
  (.createStyleSheet jss (clj->js styles)))

(defn get-classes
  [stylesheet]
  (js->clj (.-classes (.attach stylesheet)) :keywordize-keys true))

(defn get-and-attach-classes
  [styles]
  (-> styles create-stylesheet get-classes))