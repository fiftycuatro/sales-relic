(ns salesrelic.core
  (:require
    [re-frame.core :refer [dispatch dispatch-sync subscribe]]
    [bidi.bidi :as bidi]
    [com.cognitect.transit.types :as ty]
    [kee-frame.core :as k :refer [reg-controller reg-chain reg-event-db reg-event-fx]]
    [salesrelic.db :as db]
    [salesrelic.events]
    [salesrelic.subs]
    [salesrelic.domain.user :as user]
    [salesrelic.components.material-ui :as mui]
    [salesrelic.components.account-menu :refer [AccountMenu]]
    [salesrelic.components.nav-menu :refer [NavMenu]]
    [salesrelic.components.page-layout :refer [PageLayout]]
    [salesrelic.pages.home    :refer [HomePage]]
    [salesrelic.pages.special :refer [SpecialPage]]
    [salesrelic.pages.login-pane :refer [LoginPane]]
    [salesrelic.pages.users :refer [UsersPage SelectedUserPage CreateUserPage]]))

;;------------ Views -----------------------------------------------------------
(def icons
  {:home          [mui/Icon {:className "fal fa-home"  :style {:overflow :visible}}]
   :special       [mui/Icon {:className "fal fa-star"  :style {:overflow :visible}}]
   :users/index   [mui/Icon {:className "fal fa-users" :style {:overflow :visible}}]})

(defn nav-list-item
  [[handler title]]
  {:label title :icon (get icons handler) :on-click #(dispatch [:navigate-to handler])})

(def user-nav-items
  (let [items [[:home    "Home"]
               [:special "Special"]]]
    (map nav-list-item items)))

(def admin-nav-items
  (let [items [[:users/index   "Users"]]]
    (map nav-list-item items)))

(defn Page
  []
  (let [route (subscribe [:kee-frame/route])]
    (fn []
      (case (:handler @route)
        :home          [HomePage]
        :login         [LoginPane]
        :special       [SpecialPage]
        :users/index   [UsersPage]
        :users/by-id   [SelectedUserPage]
        :users/create  [CreateUserPage]

        ;; default page
        [HomePage]))))

(defn page
  []
  (let [user (subscribe [:users/current])]
    (fn []
      (let [user @user]
        [:<>
         [mui/CssBaseline]

         ;; Important that we do this here so that the CSS-in-JSS
         ;; is inserted before our manual calls to cljss/classes later
         [mui/MuiThemeProvider {:theme (clj->js (mui/theme))}
          [PageLayout {:content       [Page]
                       :content-title ""
                       :nav-menu      [NavMenu {:title "Sales Relic"
                                                :items (-> user-nav-items
                                                           (cond-> (user/is-authenticated? :admin user)
                                                                   (concat admin-nav-items)))}]
                       :account-menu  [AccountMenu {:user      user
                                                    :on-login  #(dispatch [:navigate-to :login])
                                                    :on-logout #(dispatch [:command     :logout])}]}]
          ]]))))


;;------------ Entry Point ----------------------------------------------------

;; Transit has its own UUID type so we must tell bidi how to encode
;; to a string
(extend-protocol bidi/ParameterEncoding
  ty/UUID
  (encode-parameter [s] (str s)))

(def uuid-regex #"[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")

(def my-routes ["" {"/"        :home
                    "/special" :special
                    "/login"   :login
                    "/logout"  :logout
                    "/users"   {""                   :users/index
                                "/create"            :users/create
                                ["/" [ uuid-regex :id ]] :users/by-id}}])

(reg-controller :users/index
  {:params (fn [{:keys [handler]}]
             (when (= handler :users/index)
               true))
   :start [:users/load-index]})

(reg-controller :users/by-id
  {:params (fn [{:keys [handler route-params]}]
             (when (= handler :users/by-id)
               (:id route-params)))
   :start [:users/load-profile]})

(defn ^:export run
  [debug?]
  (dispatch-sync [:users/get-current])
  (k/start! {:routes         my-routes
             :initial-db     db/default-db
             :root-component [page]
             :debug?         true}))


