(ns salesrelic.subs
  (:require
    [re-frame.core :refer [reg-sub subscribe]]
    [salesrelic.domain.user :as user]))

(reg-sub
  :users
  (fn [db _]
    (or (:users db)
        {})))

(reg-sub
  :users/current
  :<- [:users]
  (fn [users _]
    (let [current (:current users)]
      (-> current
          (assoc :authenticated (user/is-authenticated? current))))))

(reg-sub
  :users/all
  :<- [:users]
  (fn [users _]
    (:index users)))

(reg-sub
  :users/selected
  :<- [:users]
  (fn [users _]
    (:selected users)))

(reg-sub
  :users/update-loading?
  :<- [:users]
  (fn [users _]
    (:update-loading? users)))

(reg-sub
  :roles/all
  :<- [:users]
  (fn [users _]
    (:roles users)))
