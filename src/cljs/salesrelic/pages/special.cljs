(ns salesrelic.pages.special
  (:require
    [re-frame.core :refer [subscribe]]
    [salesrelic.domain.user :as user]))

(defn SpecialPage
  []
  (let [user (subscribe [:users/current])]
    (fn []
      (if (user/is-authenticated? @user)
        [:h1 "Welcome"]
        [:h1 "GO AWAY!!!"]))))