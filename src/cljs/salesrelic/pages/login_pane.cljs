(ns salesrelic.pages.login-pane
  (:require
    [reagent.core :as reagent]
    [re-frame.core :refer [dispatch subscribe]]
    [salesrelic.components.material-ui :as mui]))

(defn LoginPane
  []
  (let [username (reagent/atom "")
        password (reagent/atom "")
        route (subscribe [:kee-frame/route])]
    (fn []
      (let [is-open? (= :login (:handler @route))]
        [mui/Dialog
         {:open is-open?
          :aria-labelledby "form-dialog-title"}
         [mui/DialogTitle "User Login"]
         [mui/DialogContent
          [:div
           [mui/TextField {:label "Username"
                           :value @username
                           :on-change #(reset! username (-> % .-target .-value))
                           }]]
          [:div
           [mui/TextField {:label "Password"
                           :type "password"
                           :value @password
                           :on-change #(reset! password (-> % .-target .-value))}]]]
         [mui/DialogActions
          [mui/Button {:color "primary"
                       :onClick #(dispatch [:navigate-to :home])}
           "Cancel"]
          [mui/Button {:color "primary"
                       :variant "raised"
                       :onClick #(dispatch [:command :login {:name     @username
                                                             :password @password}])}
           "Log In"]]]))))

