(ns salesrelic.pages.home
  (:require
    [re-frame.core :refer [subscribe]]
    [salesrelic.domain.user :as user]))

(defn HomePage
  []
  (let [user (subscribe [:users/current])]
    (fn []
      (let [user @user]
        [:div
         [:h1 "Home"]
         (when (user/is-authenticated? user)
           [:h2 (str "Welcome " (:name user))])]))))
