(ns salesrelic.pages.users
  (:require
    [reagent.core :as reagent]
    [clojure.string :as string]
    [re-frame.core :refer [dispatch subscribe]]
    [salesrelic.components.material-ui :as mui]
    [salesrelic.css :as css]
    [salesrelic.components.tables :refer [Table]]
    [salesrelic.domain.user :as user]
    [kee-frame.core :as k]))

(def style
  {:root (mui/gutters
           {:paddingTop 16
            :paddingBottom 16
            :marginTop (* 3 (mui/theme :spacing :unit))})
   :user-form {:padding (* 3 (mui/theme :spacing :unit))
               :width 400}
   :user-form-buttons {:margin-top (* 3 (mui/theme :spacing :unit))
                           :display :flex
                           :justify-content :space-between}
   :dismiss-form-buttons {:margin-top (* 3 (mui/theme :spacing :unit))
                           :display :flex
                           :justify-content :center}
   :text-field {:margin (mui/theme :spacing :unit)
                :width 200}
   :button {:margin-left (mui/theme :spacing :unit)
            :margin-right (mui/theme :spacing :unit)}
   :chip   {:margin (mui/theme :spacing :unit)}
   :page-button {:position "absolute"
                 :bottom (* 2 (mui/theme :spacing :unit))
                 :right (* 2 (mui/theme :spacing :unit))}
   :icon-left {:overflow :visible
               :marginRight (mui/theme :spacing :unit)}
   })

(defn RoleChip
  [{:keys [label classes]}]
  [mui/Chip {:className (:chip classes)
             :label label}])

(defn NameCellTemplate
  [{user :data}]
  [mui/TableCell
   [:a {:href (k/path-for [:users/by-id :id (:id user)])}
    (:name user)]])

(defn RolesCellTemplate
  [{user :data
    :keys [classes]}]
  [mui/TableCell
   (for [role (:roles user)]
     ^{:key (str (:id user) "-" role "-role")}
     [RoleChip {:label role :classes classes}])])

(def user-table-model
  [{:key :name :label "Name"  :body-template NameCellTemplate}
   {:key :roles :label "Roles" :body-template RolesCellTemplate}])

(defn UsersPage
  []
  (let [users (subscribe [:users/all])
        classes (css/get-and-attach-classes style)]
    (fn []
      [mui/Paper {:className (:root classes) :elevation 4}
       [:<>
        ;; Header
         [mui/Typography {:variant "title"} "Users"]

         [mui/Button {:on-click #(dispatch [:navigate-to :users/create])
                      :className (:page-button classes)
                      :variant "fab"
                      :color "primary"}
          [mui/Icon {:className "fal fa-plus"
                     }]]

        ;; Index Table
        [Table {:model user-table-model
                :data  @users
                :classes classes}]]])))

(defn FormDismiss
  [{:keys [label color icon-class on-click classes]}]
  [:<>
   [mui/Typography {:variant "subheading"
                    :align "center"
                    :color   color} label]
   [:div {:className (:dismiss-form-buttons classes)}
    [mui/IconButton
     {:on-click on-click}
     [mui/Icon {:className icon-class
                :style {:fontSize 40}
                :color color}]]]])

(defn UserUpdateLoading
  [{:keys [status label success-label failed-label on-dimiss classes]}]
  (if (= status :loading)
    [mui/Fade
     {:in (not (nil? status))
      :style {:transitionDelay (if (nil? status) "0ms" "800ms")}}
     [:<>
      [mui/Typography {:variant "subheading"
                       :align "center"
                       :color   "primary"} label]
      [:div {:className (:dismiss-form-buttons classes)}
       [mui/CircularProgress]]]]

    ;; Else show dismiss
    [FormDismiss {:label      (if (= :success status) success-label        failed-label)
                  :color      (if (= :success status) "primary"            "error")
                  :icon-class (if (= :success status) "fal fa-check-circle" "fal fa-time-circle")
                  :on-click   on-dimiss
                  :classes classes}]))

(defn valid-password?
  [password confirmation]
  (and (not-empty password)
       (= password confirmation)))

(defn ChangePasswordForm
  [user show? classes]
  (let [loading?     (subscribe [:users/update-loading?])
        password     (reagent/atom "")
        confirmation (reagent/atom "")]
    (fn []
      (if @loading?
        [UserUpdateLoading
         {:label "Updating Password.."
          :status @loading?
          :classes classes
          :success-label "Password Updated!"
          :failed-label  "Password Update Failed"
          :on-dimiss #(do
                        (reset! show? false)
                        (dispatch [:users/clear-load]))}]

        ;; Otherwise show update form
        [:div
         [mui/Typography {:variant "subheading"} "Update Password"]
         [:div
          [mui/TextField {:label     "Password"
                          :className (:text-field classes)
                          :value     @password
                          :type      "password"
                          :on-change #(reset! password (-> % .-target .-value))}]

          [mui/TextField {:label     "Confirm Password"
                          :className (:text-field classes)
                          :value     @confirmation
                          :type      "password"
                          :on-change #(reset! confirmation (-> % .-target .-value))}]]

         [:div {:class (:user-form-buttons classes)}
          [mui/Fade
           {:in (valid-password? @password @confirmation)
            :timeout 800}
           [mui/Button
            {:className (:button classes)
             :variant   "raised"
             :color     "primary"
             :on-click  #(dispatch [:users/update-password user @password])}
            "Change Password"]]
          [mui/Button
           {:on-click  #(reset! show? false)
            :className (:button classes)
            :color     "secondary"}
           "Cancel"]]
         ]
        ))))


(defn ConfirmDialog
  [{:keys [content-text title open close action]}]
  [mui/Dialog {:open open}
   [mui/DialogTitle title]
   [mui/DialogContent
    [mui/DialogContentText content-text]]
   [mui/DialogActions
    [mui/Button {:on-click close
                 :color "secondary"} "Cancel"]
    [mui/Button {:on-click action
                 :color "primary"} "Delete User"]
    ]
   ]
  )

;; TODO CSS IS NOT QUITE RIGHT HERE ITS ATTACHING TWICE
(defn SelectedUserPage
  []
  (let [user (subscribe [:users/selected])
        logged-in-user (subscribe [:users/current])
        roles (subscribe [:roles/all])
        show-password-change? (reagent/atom false)
        classes (css/get-and-attach-classes style)
        confirm-delete? (reagent/atom false)]
    (fn []
      (let [user @user
            logged-in-user @logged-in-user]
      [:div
       [:h1 "User Profile"]
       [:h2 (:name user)]

       ;; Delete User Button
       [mui/Button {:on-click #(reset! confirm-delete? true)
                    :className (:page-button classes)
                    :variant "fab"}
        [mui/Icon {:className "fal fa-minus"}]]

       ;; Delete User Confirmation
       [ConfirmDialog {:title "Delete User Confirmation"
                       :content-text "Are you sure you want to delete user?"
                       :open @confirm-delete?
                       :close #(reset! confirm-delete? false)
                       :action #(do
                                  (reset! confirm-delete? false)
                                  (dispatch [:users/delete user]))}]

       ;; Option to update password
       (if (or (user/is-authenticated? :admin logged-in-user)
               (user/same? user logged-in-user))
         (if @show-password-change?
           [mui/Fade
            {:in @show-password-change?
             :timeout 800}
            [mui/Paper {:className (:user-form classes)
                        :elevation 4}
             [ChangePasswordForm user show-password-change? classes]]]
           [mui/Button
            {:on-click #(reset! show-password-change? true)
             :color :primary
             :size :small}
            "Update Password?"]))

       [:div
        [:h3 "Roles"]
        [mui/Select {:multiple true
                     :on-change #(dispatch [:users/update-roles user (map keyword (-> % .-target .-value))])
                     :value (map name (:roles user))
                     :input (reagent/as-element [mui/Input])
                     :renderValue  (fn [roles] (reagent/as-element
                                     [:div
                                      (for [role roles]
                                        ^{:key (str role "-role-chip2")}
                                        [RoleChip {:label (name role)
                                                   :classes classes}])]))
                     }
         (for [role @roles]
         ^{:key role} [mui/MenuItem {:key "name" :value (name role)}
                       [mui/Checkbox {:checked (not (nil? (some #{role} (:roles user))))}]
                       [mui/ListItemText {:primary (name role)}]])]

        ]]))
  ))

(defn CreateUserPage
  []
  (let [classes (css/get-and-attach-classes style)
        username (reagent/atom "")
        password (reagent/atom "")
        confirmation (reagent/atom "")
        loading?     (subscribe [:users/update-loading?])]
    (fn []
      [mui/Paper {:className (clojure.string/join " " [(:root classes) (:user-form classes)])
                  :elevation 4}
       [:<>
        ;; Header
        [:div
         [mui/Typography {:variant "title"} "Create User"]]

        (if @loading?
          [UserUpdateLoading
           {:label "Creating User..."
            :status @loading?
            :classes classes
            :success-label "User Created"
            :failed-label  "User Creation Failed"
            :on-dimiss #(do
                          (dispatch [:users/clear-load])
                          (dispatch [:navigate-to :users/index]))}]
          ;; Create User Form
          [mui/FormControl

           [mui/TextField {:label "Username"
                           :className (:text-field classes)
                           :value @username
                           :on-change #(reset! username (-> % .-target .-value))
                           }]
           [mui/TextField {:label "Password"
                           :type "password"
                           :value @password
                           :on-change #(reset! password (-> % .-target .-value))
                           :className (:text-field classes)}]
           [mui/TextField {:label "Confirm Password"
                           :type "password"
                           :value @confirmation
                           :on-change #(reset! confirmation (-> % .-target .-value))
                           :className (:text-field classes)}]
           [mui/Fade
            {:in (if (and (valid-password? @password @confirmation)
                          (not-empty @username))
                   true)
             :timeout 800}
            [mui/Button {:color "primary"
                         :variant "raised"
                         :on-click #(dispatch [:users/create @username @password])
                         :style {:margin (mui/theme :spacing :unit)}}
             "Create User"]]])
        ]])))
