(ns salesrelic.events
  (:require
    [salesrelic.db :refer [default-db]]
    [re-frame.core :refer [reg-event-db reg-event-fx dispatch reg-fx path after]]
    [day8.re-frame.http-fx]
    [ajax.core :as ajax]
    [salesrelic.domain.user :as user]
    [salesrelic.graphql :as graphql]))

(reg-event-fx
  :navigate-to
  (fn [_ args]
    {:navigate-to (vec (rest args))}))

(reg-event-fx
  :login-success
  [(path [:users :current])]
  (fn [_ [_ user]]
    {:db user
     :navigate-to [:special]}))

(reg-event-db
  :login-failure
  [(path [:users :current])]
  (fn [_ _]
    user/guest))

(reg-event-fx
  :logout-success
  [(path [:users :current])]
  (fn [_ _]
    {:db user/guest
     :navigate-to [:home]}))

(reg-event-fx
  :logout-failure
  [(path [:users :current])]
  (fn [_ _]
    {:db user/guest
     :navigate-to [:home]}))

;;------------------------
;; User Events
;;------------------------

(reg-event-fx
  :users/get-current
  (fn [_ _]
    {:graphql {:query graphql/current-user-query
               :on-success [:current-user-success]}}))

(reg-event-db
  :current-user-success
  [(path [:users :current])]
  (fn [_ [_ {data :data}]]
    (:user data)))

(reg-event-fx
  :users/load-index
  (fn [_ _]
      {:graphql {:query graphql/users-index-query
                 :on-success [:users-success]}}))

(reg-event-db
  :users-success
  [(path [:users])]
  (fn [users [_ {data :data}]]
    (if (:users data)
      (assoc users :index (:users data)))))

(reg-event-fx
  :users/load-profile
  (fn [{:keys [db]} [_ id]]
    {:db (dissoc db :selected-user :user-update-loading?)
     :graphql {:query graphql/user-profile-query
               :variables {:id id}
               :on-success [:users-profile-success]}}))

(reg-event-db
  :users-profile-success
  [(path [:users])]
  (fn [users [_ {data :data}]]
    (-> users
        (cond-> (:user data)  (assoc :selected (:user data)))
        (cond-> (:roles data) (assoc :roles    (:roles data))))))

(reg-event-fx
  :users/update-password
  [(path [:users])]
  (fn [{:keys [db]} [_ user password]]
    {:db (assoc db :update-loading? :loading)
     :graphql {:query graphql/update-user-password-mutation
               :variables {:id (:id user) :password password}
               :on-success [:users-password-update-complete]
               :on-failure [:users-password-update-complete]
               }}))

(reg-event-fx
  :users-password-update-complete
  [(path [:users])]
  (fn [{:keys [db]} [_ {data :data}]]
    (if (:updateUserPassword data)
      {:db (assoc db :update-loading? :success)}
      {:db (assoc db :update-loading? :failed)})))

(reg-event-db
  :users/clear-load
  [(path [:users])]
  (fn [users _]
    (dissoc users :update-loading?)))

(reg-event-fx
  :users/create
  [(path [:users])]
  (fn [{:keys [db]} [_ username password]]
    {:db (assoc db :update-loading? :loading)
     :graphql {:query graphql/create-user-mutation
               :variables {:username username :password password}
               :on-success [:users-create-complete]
               :on-failure [:users-create-complete]}}))

(reg-event-fx
  :users-create-complete
  [(path [:users])]
  (fn [{:keys [db]} [_ {data :data}]]
    (if (:createUser data)
      {:db (assoc db :selected (:createUser data)
                     :update-loading? :success)}
      {:db (assoc db :update-loading? :failed)})))

(reg-event-fx
  :users/delete
  (fn [_ [_ user]]
    {:graphql {:query graphql/delete-user-mutation
               :variables {:id (:id user)}
               :on-success [:users-delete-complete user]}}))

(reg-event-fx
  :users-delete-complete
  [(path [:users :index])]
  (fn [{:keys [db]} [_ user {data :data}]]
    (if (:deleteUser data)
      {:db (filter #(not (= (:id %) (:id user))) db)
       :navigate-to [:users/index]}
      ))
  )

(reg-event-fx
  :users/update-roles
  (fn [_ [_ user roles]]
    {:graphql {:query graphql/update-user-roles-mutation
               :variables {:id (:id user)
                           :roles roles}
               :on-success [:users-profile-success]}}))
;;--------
;; Utilities

(defn success-event
  [cmd]
  (keyword (str (name cmd) "-success")))

(defn failure-event
  [cmd]
  (keyword (str (name cmd) "-failure")))

(reg-event-fx
  :command
  (fn [_ [_ command args]]
    {:http-xhrio {:method :post
                  :uri    "/api/command/"
                  :timeout 8000
                  :params          {:command command
                                    :args    args}
                  :format          (ajax/transit-request-format)
                  :response-format (ajax/transit-response-format)
                  :on-success [(success-event command)]
                  :on-failure [(failure-event command)]}}))
