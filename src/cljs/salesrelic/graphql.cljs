(ns salesrelic.graphql
  (:require [day8.re-frame.http-fx]
            [ajax.core :as ajax]
            [re-frame.core :refer [reg-event-fx dispatch reg-fx]]))

(def current-user-query
  "{
     user {
       id
       name
       roles
     }
   }")

(def users-index-query
  "{
     users {
       id
       name
       roles
     }
   }")

(def user-profile-query
  "query UserProfileQuery($id: UUID) {
     user(id: $id) {
       id
       name
       roles
     }

     roles
  }")



(def update-user-password-mutation
  "mutation UpdateUserPassword($id: UUID, $password: String) {
     updateUserPassword(id: $id, password: $password) {
       id
     }
   }")

(def create-user-mutation
  "mutation CreateUser($username: String, $password: String) {
    createUser(name: $username, password: $password) {
       id
       name
       roles
    }
  }")

(def delete-user-mutation
  "mutation DeleteUser($id: UUID) {
     deleteUser(id: $id)
   }")

(def update-user-roles-mutation
  "mutation UpdateUserRoles($id: UUID, $roles: [role]) {
    user: updateUserRoles(id: $id, roles: $roles) {
      id
      name
      roles
    }
  }")

(reg-fx
  :graphql
  (fn [args]
    (dispatch [::query args])))

(reg-event-fx
  ::query
  (fn [_ [_ {:keys [query variables on-success on-failure]}]]
    {:http-xhrio (-> {:method :post
                      :uri    "/graphql/"
                      :timeout 8000
                      :params          (-> {:query query}
                                           (cond-> variables (assoc :variables variables)))
                      :format          (ajax/transit-request-format)
                      :response-format (ajax/transit-response-format)}
                     (cond-> on-success (assoc :on-success on-success))
                     (cond-> on-failure (assoc :on-failure on-failure)))}))
