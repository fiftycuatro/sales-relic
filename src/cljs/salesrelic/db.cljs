(ns salesrelic.db
  (:require
    [salesrelic.domain.user :as user]))

(def default-db
  {:users    {:current user/guest}})