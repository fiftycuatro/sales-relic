(ns salesrelic.roles
  (:require
    [salesrelic.database.core :as db]))

(defn find-by
  [{:salesrelic/keys [db]} role]
  (cond
    (:id role)   (db/role-by-id db role)
    (:name role) (db/role-by-name db role)
    :default nil))

(defn create<!
  [{:salesrelic/keys [db]} vals]
  (if-let [role (db/role-by-name db vals)]
    role
    (first (db/insert-role<! db vals))))

(defn all
[{:salesrelic/keys [db]}]
  (map :name (db/all-roles db)))
