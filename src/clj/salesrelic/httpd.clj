(ns salesrelic.httpd
  (:require 
    [compojure.handler]
    [compojure.core :as compojure :refer [GET POST DELETE]]
    [compojure.route :refer [resources not-found]]
    [compojure.response :as compojure-response]
    [ring.util.response :refer [response redirect status get-header]]
    [ring.middleware.session :refer [wrap-session]]
    [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
    [ring.middleware.transit :refer [wrap-transit-response wrap-transit-body]]
    [ring.middleware.gzip :refer [wrap-gzip]]
    [ring.adapter.jetty :refer [run-jetty]]
    [ring.util.request :refer [body-string]]
    [buddy.auth.accessrules :refer [restrict]]
    [clojure.tools.logging :as log]
    [integrant.core :as ig]
    [salesrelic.handlers :refer [http-command-handler]]

    ;; Import namespaces which register command handlers
    [salesrelic.auth :refer [wrap-authentication wrap-authorization wrap-user]]
    [salesrelic.graphql]
    ))

;;----------- Utils -----------------------------------------------------------
;;
(defn default-response
  [body]
  (fn [request]
      (-> (compojure-response/render body request)
          (status 200)
          (cond-> (= (:request-method request) :head) (assoc :body nil)))))

(defn transit-content? [request]
  (if-let [type (get-header request "Accept")]
    (not (empty? (re-find #"^application/transit\+(json|msgpack)" type)))))

(defn transit-body? [request]
  (if-let [type (get-header request "Content-Type")]
    (not (empty? (re-find #"^application/transit\+(json|msgpack)" type)))))

;; Tired. This can be cleaned up
(defn wrap-transit-or-json-response
  [handler opts]
  (let [json-handler (wrap-json-response handler (or (:json opts) {}))
        transit-handler (wrap-transit-response handler (or (:transit opts) {}))]
    (fn [request]
      (if (transit-content? request)
        (transit-handler request)
        (json-handler request)))))

(defn wrap-transit-or-json-body
  [handler opts]
  (let [json-handler (wrap-json-body handler (or (:json opts) {}))
        transit-handler (wrap-transit-body handler (or (:transit opts) {}))]
    (fn [request]
      (if (transit-body? request)
        (transit-handler request)
        (json-handler request)))))


;;----------- Handlers ---------------------------------------------------------
;;
(defn index-resource
  [{:salesrelic.httpd/keys [dev-mode]}]
  (clojure.java.io/resource (if dev-mode
                              "public/index-dev.html"
                              "public/index.html")))

;;----------- Middleware ------------------------------------------------------
;;
(defn wrap-map
  [handler pairs]
  (fn [request]
    (handler (merge request pairs))))

;;----------- Routes ----------------------------------------------------------
;;
(def resource-routes
  (compojure/routes
    (GET "/" [] index-resource)
    (resources "/")
    (default-response index-resource)))

(def api-routes
  (compojure/context "/api" []
    (-> (compojure/routes
          (POST "/command/" [] http-command-handler))

        ;; API Middleware
        compojure.handler/api
        (wrap-transit-response {:encoding :json :opts {}})
        (wrap-transit-body {:keywords? true :opts {}})
        wrap-user)))

(def graphql-route
  (-> (compojure/routes
        (POST "/graphql/" [] (fn [req]
                              (http-command-handler (assoc req :body
                                                               {:command :graphql
                                                                :args     (:body req)})))))

      ;; GraphQL Middleware
      compojure.handler/api
      (wrap-transit-or-json-response {:transit {:encoding :json :opts {}}})
      (wrap-transit-or-json-body {:json {:keywords? true}})
      wrap-user))

(defn app-handler
  [config]
  (-> (compojure/routes
        api-routes
        graphql-route
        resource-routes)

      ;; Global Middleware
      wrap-authentication
      wrap-authorization
      (wrap-map config)
      (wrap-session)
      (wrap-gzip)))

;;----------- Http Server -----------------------------------------------------
;;
(defmethod ig/init-key :salesrelic/httpd
  [_ {:salesrelic.httpd/keys [port] :as config}]
  (log/info "Starting on port " port)
  {:server (run-jetty (app-handler config) {:port port :join? false})})

(defmethod ig/halt-key! :salesrelic/httpd
  [_ {:keys [server]}]
  (log/info "Stopping")
  (try
    (.stop server)
    (catch Exception e
      (log/error e "Failed to properly close http listener"))))
