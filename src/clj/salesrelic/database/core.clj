(ns salesrelic.database.core
  (:require
    [clojure.tools.logging :as log]
    [ragtime.core :as ragtime]
    [ragtime.jdbc]
    [ragtime.protocols]
    [ragtime.clj.core]
    [integrant.core :as ig]
    [hugsql.core :as hugsql]
    [clojure.spec.alpha :as s]
    [spec-tools.core :as st]
    [spec-tools.spec :as spec]
    [spec-tools.transform :as stt]
    [camel-snake-kebab.core :refer [->kebab-case ->snake_case]]

    [salesrelic.domain.role :as role]
    [salesrelic.domain.user :as user]))


;; -------- Transformations ---------------------------------------------------
;;
(defn map-keys
  [f]
  (fn [_ x]
    (if (map? x)
      (into {} (map (fn [[k v]] [(f k) v]) x))
      x)))

(def map-keys->kebab-case (map-keys ->kebab-case))
(def map-keys->snake_case (map-keys ->snake_case))

(def postgres-decoders
  {:keyword stt/string->keyword
   :map map-keys->kebab-case})

(def postgres-encoders
  {:keyword stt/keyword->string
   :map map-keys->snake_case})

(defn any->postgres
  [_ x]
  (if (keyword? x)
    (name x)
    x))

(def postgres-transformer
  (st/type-transformer
    {:name :postgres
     :decoders postgres-decoders
     :encoders postgres-encoders
     :default-encoder any->postgres}))

;; -------- Helpers -----------------------------------------------------------
;;
(def sql-resource-path "salesrelic/database/sql/")


(defn sql-file-name
  [schema]
  (str sql-resource-path
       (->snake_case (name schema))
       ".sql"))

(defn load-queries [schema]
  (let [{snips true
         fns   false}
        (group-by
          #(-> % second :snip? boolean)
          (hugsql/map-of-db-fns (sql-file-name schema)))]
    {:snips snips
     :fns fns}))

(defn encode-or-assert
  [spec data]
  (if (s/valid? spec data)
    (let [encoded (st/encode spec data postgres-transformer)]
      (if (= encoded ::s/invalid)
        (throw (AssertionError. (st/explain spec data)))
        encoded))
    (throw (AssertionError. (s/explain spec data)))))

(defn decode-or-assert
  [spec data]
  (let [decoded-data (st/decode spec data postgres-transformer)]
    (if (= decoded-data ::s/invalid)
      (throw (AssertionError. (st/explain spec data)))
      decoded-data)))

(defn transform
  [f meta-spec-key default]
  (fn
    [meta specs]
    (if-let [spec (->> (get meta meta-spec-key)
                       (get specs))]
      (partial f spec)
      default)))

;; Unfortunately only supports 1 level deep at the moment
;; might just want to remove default transformation at this point
;; as without recursion its not that useful
(s/def ::any-map? (st/spec (s/map-of spec/any? spec/any?)))
(s/def ::default-encode ::any-map?)
(s/def ::default-decode (st/spec (s/or :single ::any-map?
                                       :coll   (s/coll-of ::any-map?)
                                       :int    number?
                                       :nil    nil?)))

(defn encode-any
  [data] (st/encode ::default-encode data postgres-transformer))
(defn decode-any
  [data] (st/decode ::default-decode data postgres-transformer))

(def db-encode (transform encode-or-assert :params-spec encode-any))
(def db-decode (transform decode-or-assert :return-spec decode-any))

(defn def-db-fns
  ([schema] (def-db-fns schema {}))
  ([schema specs]
   `(let [{snips# :snips fns#   :fns} (load-queries '~schema)]
      (doseq [[id# {fn# :fn}] snips#]
        (intern *ns* (symbol (name id#)) fn#))
      (doseq [[id# {fn# :fn meta# :meta}] fns#]
        (intern *ns* (symbol (name id#))
                (let [encode# (db-encode meta# ~specs)
                      decode# (db-decode meta# ~specs)]
                  (fn
                    ([db#] (decode# (fn# db#)))
                    ([db# params#]
                     (decode# (fn# db# (encode# params#))))
                    ([db# params# opts# & command-opts#]
                     (decode# (apply fn# db# (encode# params#) opts# command-opts#))))))))))

(defmacro bind-db-fns [& args] (apply def-db-fns args))

;; -------- Define all database functions -------------------------------------
;;
(def db-data-specs
  {:user        ::user/user
   :users       ::user/users
   :user-or-nil ::user/user-or-nil
   :role        ::role/role
   :roles       ::role/roles
   :role-or-nil ::role/role-or-nil})

(bind-db-fns :procedures)
(bind-db-fns :users db-data-specs)
(bind-db-fns :roles db-data-specs)
(bind-db-fns :users-roles db-data-specs)

;; -------- Database Component ------------------------------------------------
;;
(defn apply-migrations
  [conn {:salesrelic.db/keys [migration-resource-dir]}]
  (let [db      (ragtime.jdbc/sql-database conn)
        ms      (ragtime.jdbc/load-resources migration-resource-dir)
        idx     (ragtime/into-index ms)
        cur-ids (ragtime.protocols/applied-migration-ids db)]
    (log/info "Current migrations applied: " cur-ids)
    (if (< (count cur-ids) (count idx))
      (do
        (ragtime/migrate-all db idx ms)
        (log/info "Migrations applied: " (-> (ragtime.protocols/applied-migration-ids db)
                                             (clojure.set/difference (set cur-ids))
                                             sort))))))

(defmethod ig/init-key :salesrelic/db
  [_ {:salesrelic.db/keys [host port name username password] :as config}]
  (let [db {:dbtype   "postgresql"
            :dbname   name
            :host     host
            :port     port
            :user     username
            :password password}]
    (log/info "Starting Database")
    (apply-migrations db config)
    db))

(comment

  (def db {:dbtype   "postgresql"
           :dbname   "postgres"
           :host     "localhost"
           :port     "5432"
           :user     "postgres"
           :password "password"})

  (all-users db)
  (user-by-id db {:id 2})
  (insert-user<! db {:name "Phillip" :password-hash "12313"})


  ()
)