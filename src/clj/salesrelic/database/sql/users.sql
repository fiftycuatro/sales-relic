-- ----- users ----------------------------------------------------------------
--

-- :name create-users-table :!
CREATE TABLE users (
    id            uuid DEFAULT uuid_generate_v4 (),
    name          varchar(40) NOT NULL CHECK (name <> ''),
    password_hash varchar(256) NOT NULL,
    created_at    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
CREATE TRIGGER update_updated_at_users
    BEFORE UPDATE ON users FOR EACH ROW EXECUTE
    PROCEDURE update_updated_at_column();

-- :name drop-users-table :!
DROP TABLE users;

-- :name insert-user<! :<!
-- :doc Insert user and returned created user
-- :meta {:params-spec :user :return-spec :users}
INSERT INTO users (name, password_hash)
VALUES (:name, :password_hash)
RETURNING *;

-- :name update-user-password<! :<!
-- :doc Update an existing users password
-- :meta {:params-spec :user}
UPDATE users
SET password_hash = :password_hash
WHERE id = :id
RETURNING *;

-- :name delete-user! :! :n
-- :doc Deletes an existing users
-- :meta {:params-spec :user}
DELETE FROM users
WHERE id = :id

-- :name all-users :? :*
-- :meta {:return-spec :users}
SELECT * FROM users;

-- :name user-by-id :? :1
-- :doc Queries for user by id
-- :meta {:params-spec :user :return-spec :user-or-nil}
SELECT *
FROM users
WHERE id = :id

-- :name user-by-name :? :1
-- :doc Queries for user by name
-- :meta {:params-spec :user :return-spec :user-or-nil}
SELECT *
FROM users
WHERE name = :name