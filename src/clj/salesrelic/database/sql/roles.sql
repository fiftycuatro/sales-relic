-- ----- roles ----------------------------------------------------------------
--

-- :name create-roles-table :!
CREATE TABLE roles (
    id         serial PRIMARY KEY,
    name       varchar(40) NOT NULL UNIQUE,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- :name drop-roles-table :!
DROP TABLE roles;

-- :name insert-role<! :<!
-- :doc Insert role
-- :meta {:params-spec :role :return-spec :roles}
INSERT INTO roles (name)
VALUES (:name)
RETURNING *;

-- :name all-roles :?
-- :meta {:return-spec :roles}
SELECT *
FROM roles;

-- :name role-by-id :? :1
-- :meta {:params-spec :role :return-spec :role-or-nil}
SELECT *
FROM roles
WHERE id = :id;

-- :name role-by-name :? :1
-- :meta {:params-spec :role :return-spec :role-or-nil}
SELECT *
FROM roles
WHERE name = :name;