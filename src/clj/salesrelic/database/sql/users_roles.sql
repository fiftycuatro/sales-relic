-- ----- users-roles ----------------------------------------------------------
--

-- :name create-users-roles-table :!
CREATE TABLE users_roles (
    users_id uuid REFERENCES users(id) ON DELETE CASCADE,
    roles_id integer REFERENCES roles(id) ON DELETE CASCADE,
    PRIMARY KEY (users_id, roles_id)
);

-- :name drop-users-roles-table :!
DROP TABLE users_roles;

-- :name add-role-to-user-relationship! :!
-- :doc Add role/user relationship
INSERT INTO users_roles (users_id, roles_id)
VALUES (:users_id, :roles_id);

-- :name delete-role-to-user-relationship! :!
-- :doc Add role/user relationship
DELETE FROM users_roles
WHERE users_id = :users_id AND roles_id = :roles_id;


-- :name roles-for-user :? :*
-- :doc Returns roles for user
-- :meta {:return-spec :roles}
SELECT roles.*
FROM users_roles
INNER JOIN roles ON users_roles.roles_id = roles.id
WHERE users_id = :id;