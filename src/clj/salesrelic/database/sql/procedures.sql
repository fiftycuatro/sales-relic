-- Postgresql Functions

-- :name create-updated-at-procedure :!
-- :doc  Create procedure to update updated_at column
CREATE OR REPLACE FUNCTION update_updated_at_column()
        RETURNS TRIGGER AS '
    BEGIN
        NEW.updated_at = NOW();
        RETURN NEW;
    END;
' LANGUAGE 'plpgsql';


-- :name drop-updated-at-procedure :!
DROP FUNCTION update_updated_at_column();

-- :name install-uuid-extension :!
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";