(ns salesrelic.handlers
  (:require
    [clojure.tools.logging :as log]))

(defmulti http-command-handler
          (fn [req] (get-in req [:body :command])))

(defmethod http-command-handler :default
  [req]
  {:status 400
   :body {:msg "Command not found"}})