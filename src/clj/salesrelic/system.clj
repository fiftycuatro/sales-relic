(ns salesrelic.system
  (:require
    [aero.core :as aero]
    [clojure.java.io :as io]
    [integrant.core :as ig]

    ;; Need to import all namespaces that register init-key methods
    [salesrelic.graphql]
    [salesrelic.httpd]
    [salesrelic.database.core]))

(defmethod aero/reader 'ig/ref [_ _ value]
  (ig/ref value))


(defn resource-or-root-resolver
  "First attempts to resolve as a resource then from root"
  [source include]
  (let [include (or (io/resource include)
                    include)]
    (if (string? include)
      (aero/root-resolver source include)
      include)))

(defn config
  "Read EDN config, with the given profile."
  [profile]
  (aero/read-config (io/resource "config.edn") {:profile profile
                                                :resolver resource-or-root-resolver}))

(defn new-system
  "Constructs a new system, configured with the given profile"
  [profile]
  (let [res (:ig/system (config profile))]
    (ig/load-namespaces res)
    res))

