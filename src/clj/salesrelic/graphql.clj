(ns salesrelic.graphql
  (:require
    [com.walmartlabs.lacinia :refer [execute]]
    [com.walmartlabs.lacinia.util :refer [attach-resolvers]]
    [com.walmartlabs.lacinia.schema :as schema]
    [clojure.tools.logging :as log]
    [integrant.core :as ig]
    [salesrelic.handlers :refer [http-command-handler]]
    [salesrelic.users :as users]
    [salesrelic.roles :as roles]))

;; -------- Query Methods -----------------------------------------------------
;;
(defn string->uuid
  [ctx args val]
  (prn "UUID")
  (prn args)
  (prn val))

;; -------- Query Methods -----------------------------------------------------
;;

(defn get-all-users
  [ctx _ _]
  (users/all ctx))

(defn get-user
  [ctx args _]
  (or (users/find-by ctx args)
      (:user ctx)))

(defn get-role
  [ctx args _]
  (roles/find-by ctx args))

(defn get-user-roles
  [ctx _ user]
  (if (:roles user)
    (:roles user)
    (:roles (users/with-roles ctx user))))

(defn update-user-roles!
  [ctx args _]
  (let [roles (set (:roles args))
        user  {:id (:id args)}]
    (users/update-user-roles<! ctx user roles)))

(defn all-roles
  [ctx _ _]
  (roles/all ctx))

;; -------- Mutation Methods -----------------------------------------------------
;;

(defn create-user!
  [ctx args _]
  (users/create<! ctx args))

(defn update-user-password!
  [ctx {:keys [id password]} _]
  (let [user {:id id}]
    (users/update-password<! ctx user password)))

(defn delete-user!
  [ctx args _]
  (= 1 (users/delete! ctx args)))

(defn execute-query
  [gql query variables ctx]
  (let [{:keys [schema]} gql]
    (execute schema query variables ctx)))

;; ---------- GraphQL Endpoint ------------------------------------------------
;;
(defmethod ig/init-key :salesrelic/graphql
  [_ {:salesrelic.graphql/keys [schema]}]

  {:schema (-> schema
               ;; Add any custom scalar type conformers
               (merge {:scalars
                       {:UUID
                        {:parse (schema/as-conformer #(if (instance? java.util.UUID %) % (java.util.UUID/fromString %)))
                         :serialize (schema/as-conformer #(identity %))
                         }}})

               ;; Add resolve functions
               (attach-resolvers {:uuid                 string->uuid
                                  :user                 get-user
                                  :users                get-all-users
                                  :roles                get-user-roles
                                  :all-roles            all-roles
                                  :create-user          create-user!
                                  :update-user-password update-user-password!
                                  :update-user-roles    update-user-roles!
                                  :delete-user          delete-user!})
               schema/compile)})

;; ---------- HTTP Handler ----------------------------------------------------
;;
(defmethod http-command-handler :graphql
  [{{{:keys [query variables]} :args} :body
    :salesrelic/keys [graphql] :as ctx}]
  {:status  200
   :body (execute-query graphql query variables ctx)})
