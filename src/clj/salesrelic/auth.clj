(ns salesrelic.auth
  (:require
    [buddy.auth.middleware :as buddy]
    [buddy.auth.backends.session :refer [session-backend]]
    [salesrelic.handlers :refer [http-command-handler]]
    [salesrelic.users :as user]
    [salesrelic.domain.user :refer [is-authenticated? guest ->public]]
    ))

;; -------- Auth Backend ------------------------------------------------------
;;
(def backend (session-backend))

;; ---------- Middleware ------------------------------------------------------
;;
(defn wrap-user
  [handler]
  (fn [{:keys [identity] :as req}]
    (if identity
      (let [user (user/find-by req {:id identity})]
        (handler (assoc req :user (user/with-roles req user))))
      (handler (assoc req :user guest)))))

(defn wrap-authentication
  [handler]
  (buddy/wrap-authentication handler backend))

(defn wrap-authorization
  [handler]
  (buddy/wrap-authorization handler backend))

;; ---------- HTTP Handlers ---------------------------------------------------
;;
(defn get-current-user
  [{:keys [user]}]
  (if (and user
           (is-authenticated? user))
    user))

(defmethod http-command-handler :login
  [{:keys [session]
    {args :args} :body :as ctx}]
  (if-let [current-user (get-current-user ctx)]
    ;; User already logged in
    {:status 200 :body current-user}

    ;; Attempt to login
    (if-let [user (user/by-name-and-password ctx args)]
      ;; Login success
      (-> {:status  200
           :body    (user/with-roles ctx user)
           :session session}
          (assoc-in [:session :identity] (:id user)))

      ;; Login error
      {:status 401 :body "Unrecognized username or password"})))

(defmethod http-command-handler :logout
  [{:keys [session cookies]}]
  (assoc {:status 200}
    :session (dissoc session :identity)

    ;; TODO: I don't think this is valid anymore. Remove?
    :cookies (assoc cookies :roles {:value "" :max-age 0})))