(ns salesrelic.users
  (:require
    [buddy.hashers :as hashers]
    [clojure.java.jdbc :as jdbc]
    [salesrelic.database.core :as db]
    [salesrelic.roles :as roles]
    [salesrelic.domain.user :as user]
    [clojure.set :as set]))

(defn password->hash
  [{:keys [password] :as vals}]
     (assoc vals :password-hash (hashers/encrypt password)))

(defn all
  [{:salesrelic/keys [db]}]
  (user/->public
    (db/all-users db)))

(defn ^:private find-by-non-public
  [{:salesrelic/keys [db]} query]
    (cond
      (:id query)   (db/user-by-id db query)
      (:name query) (db/user-by-name db query)
      :default nil))

(defn find-by
  [ctx query]
  (user/->public (find-by-non-public ctx query)))

(defn exists?
  [ctx query]
  (not (nil? (find-by ctx query))))

(defn with-roles
  [{:salesrelic/keys [db]} user]
  (let [roles (or (db/roles-for-user db user)
                  [])]
    (user/->public
      (assoc user :roles (into #{} (map :name roles))))))

(defn by-name-and-password
  [ctx query]
  (if-let [user (find-by-non-public ctx query)]
    (let [query-password (:password query)
          password-hash (:password-hash user)]
    (if (hashers/check query-password password-hash)
      (user/->public user)))))

(defn attach-role!
  [{:salesrelic/keys [db]} user role]
  (let [rel {:users-id (:id user)
             :roles-id (:id role)}]
       (db/add-role-to-user-relationship! db rel)))

(defn detach-role!
  [{:salesrelic/keys [db]} user role]
  (let [rel {:users-id (:id user)
             :roles-id (:id role)}]
    (db/delete-role-to-user-relationship! db rel)))

(defn create<!
  [{:salesrelic/keys [db] :as ctx} vals]
    (if-let [user (find-by ctx vals)]
      user
      ;; if user not found then insert
      (jdbc/with-db-transaction [tx db]
        (let [ctx (assoc ctx :salesrelic/db tx)
              user (first (db/insert-user<! db (password->hash vals)))
              role (roles/find-by ctx {:name :user})]
          (attach-role! ctx user role)
          user))))

(defn delete!
  [{current :user
    :salesrelic/keys [db] :as ctx} user]
  (if-let [user (find-by ctx user)]
    (when (and (user/is-authenticated? :admin current)
               (not (user/same? current user)))
      (db/delete-user! db user))))

(defn update-password<!
  [{current :user
    :salesrelic/keys [db] :as ctx} user password]
  (if-let [user (find-by ctx user)]
    (when (or (user/is-authenticated? :admin current)
              (user/same? current user))
      (user/->public
        (first (db/update-user-password<! db (-> user
                                                 (assoc :password password)
                                                 password->hash)))))))

(defn update-user-roles<!
  [{current :user
    :salesrelic/keys [db] :as ctx} user roles]
  (if-let [user (with-roles ctx (find-by ctx user))]
    (when (user/is-authenticated? :admin current)
      (let [existing (:roles user)
            to-add (set/difference roles existing)
            to-remove (set/difference existing roles)
            ;; cannot remove own :admin role
            to-remove (if (user/same? current user)
                        (remove #{:admin} to-remove)
                        to-remove)]
        (doseq [role-name to-add]
          (attach-role! ctx user (roles/find-by ctx {:name role-name})))
        (doseq [role-name to-remove]
          (detach-role! ctx user (roles/find-by ctx {:name role-name}))))))
  ;; Return updated user
  (user/->public
    (with-roles ctx (find-by ctx user))))

(comment

  (def db {:dbtype   "postgresql"
           :dbname   "postgres"
           :host     "localhost"
           :port     "5432"
           :user     "postgres"
           :password "password"})
  (def ctx {:salesrelic/db db})
  (def admin (find-by ctx {:id 1}))

  (with-roles ctx admin)

  (create<! ctx {:name "nonadmin" :password "1235"})

  (find-by ctx {:id 3})
  )
