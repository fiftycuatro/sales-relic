(ns salesrelic.domain.user
  (:require
    [clojure.spec.alpha :as s]
    [spec-tools.core :as st]
    [spec-tools.transform :as stt]
    [salesrelic.domain.role :as role]))

;;-------- Specs --------------------------------------------------------------
;;

(s/def ::id uuid?)
(s/def ::name string?)
(s/def ::password_hash string?)
(s/def ::roles (st/spec (s/coll-of ::role/name)))
(s/def ::user (st/spec (s/or :with-id (st/spec (s/keys :req-un [::id]
                                                       :opt-un [::name ::roles ::password-hash]))
                             :with-name (st/spec (s/keys :req-un [::name]
                                                         :opt-un [::id ::roles ::password-hash])))))
(s/def ::users (st/spec (s/coll-of ::user)))

(s/def ::user-or-nil (st/spec (s/or :user ::user
                                    :nil nil?)))

(s/def ::user-public (st/spec (s/keys :opt-un [::id ::name ::roles])))
(s/def ::users-public
  (s/or :single ::user-public
        :coll   (s/coll-of ::user-public)
        :nil nil?))

(def guest {:name "Guest" :roles #{:guest}})

(def user-transformer
  (st/type-transformer
    {:name :postgres
     :decoders stt/strip-extra-keys-type-decoders
     :default-encoder stt/any->any}))

(defn ->public
  [user]
  (st/decode ::users-public user user-transformer))

(defn is-authenticated?
  ([user] (is-authenticated? :user user))
  ([role user]
   (get (set (:roles user)) role)))

(defn same?
  [user1 user2]
  (= (:id user1) (:id user2)))

(comment

  (def user {:id 121 :name "Phillip" :roles [{:id 123 :name "user"}] :password-hash "1312312"})

  (->public user)
  )