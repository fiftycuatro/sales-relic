(ns salesrelic.domain.role
  (:require
    [clojure.spec.alpha :as s]
    [spec-tools.core :as st]
    [spec-tools.spec :as spec]))

;;-------- Specs --------------------------------------------------------------
;;
(s/def ::id int?)
(s/def ::name (st/spec (s/and spec/keyword? #{:user :admin :guest})))
(s/def ::role (st/spec (s/keys :opt-un [::id ::name])))
(s/def ::roles (st/spec (s/coll-of ::role)))

(s/def ::role-or-nil (s/or :role ::role
                           :nil nil?))
