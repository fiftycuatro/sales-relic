(ns user
  (:require
    [com.stuartsierra.component :as component]
    [clojure.tools.namespace.repl :refer [refresh]]
    [figwheel-sidecar.system :as sys]
    [salesrelic.httpd :as core]
    [salesrelic.system :as system]
    [integrant.core :as ig]
    [integrant.repl :as repl]))

(defn deep-merge [v & vs]
  (letfn [(rec-merge [v1 v2]
            (if (and (map? v1) (map? v2))
              (merge-with deep-merge v1 v2)
              v2))]
    (if (some identity vs)
      (reduce #(rec-merge %1 %2) v vs)
      v)))


(def figwheel-system nil)
(defmethod ig/init-key :salesrelic/figwheel
  [_ config]
  (let [figwheel (component/start
                   (sys/figwheel-system (deep-merge (sys/fetch-config)
                                                    config)))]

    (alter-var-root #'figwheel-system (constantly figwheel))
    {:figwheel figwheel}))

(defmethod ig/halt-key! :salesrelic/figwheel
  [_ {:keys [figwheel]}]
  (component/stop figwheel))

(repl/set-prep! #(system/new-system :dev))

(defn go
  []
  (repl/go))

(defn reset
  []
  (repl/reset))

(defn cljs-repl
  []
  (when figwheel-system
    (sys/cljs-repl figwheel-system)))



