(ns salesrelic.devtools
  (:require [devtools.core :as devtools]))


(devtools/install!)
(.log js/console {:js "DevTools Loaded"})

(enable-console-print!)
