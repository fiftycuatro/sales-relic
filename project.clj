(defproject salesrelic "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "MIT License"
            :url "https://opensource.org/licenses/mit-license.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [com.stuartsierra/component "0.3.2"]
                 [org.clojure/tools.cli "0.3.7"]
                 [org.clojure/tools.logging "0.4.1"]
                 [ch.qos.logback/logback-classic "1.2.3"]
                 [buddy "2.0.0"]
                 [org.clojure/core.async "0.4.474"]
                 [com.cognitect/transit-clj "0.8.309"]
                 [aero "1.1.3"]
                 [integrant "0.6.3"]
                 [integrant/repl "0.3.1"]
                 [metosin/spec-tools "0.7.0-SNAPSHOT"]
                 [camel-snake-kebab "0.4.0"]

                 ;; Had to add
                 [org.clojure/test.check "0.10.0-alpha2"]

                 ;; Database Releated
                 [ragtime "0.7.2"]
                 [com.mjachimowicz/ragtime-clj "0.1.0"]
                 [org.clojure/java.jdbc "0.7.6"]
                 [org.postgresql/postgresql "9.4.1212"]
                 [com.layerware/hugsql "0.4.8"]
                 [com.walmartlabs/lacinia "0.26.0"]

                 ;; HTTP server deps 
                 [compojure/compojure "1.6.1"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-jetty-adapter "1.6.3"]
                 [ring/ring-json "0.4.0"]
                 [ring-transit "0.1.6"]
                 [bk/ring-gzip "0.3.0"]
                
                 ;; Front end libs
                 [reagent "0.8.0" :exclusions [cljsjs/react cljsjs/react-dom]]
                 [re-frame "0.10.5"]
                 [day8.re-frame/http-fx "0.1.6"]
                 [binaryage/devtools "0.9.10"]
                 [bidi "2.1.3"]
                 [kibu/pushy "0.3.8"]
                 [cljs-http "0.1.45"]
                 [com.cognitect/transit-cljs "0.8.256"]
                 [cljsjs/material-ui "1.4.0-0"]
                 [kee-frame "0.2.2"]]

  :source-paths ["src/clj" "src/cljc"]
  :test-paths ["test/clj"]
  :plugins [[lein-cljsbuild "1.1.7"]
            [lein-ancient "0.6.10"]]
  :clean-targets ^{:protect false} [:target-path "resources/public/js/app/"] 
  :profiles {:uberjar {:aot        :all
                       :prep-tasks ["compile" ["cljsbuild" "once" "min"]]
                       :main       salesrelic.main}
             
             :dev {:source-paths ["src/clj" "src/cljc" "dev/clj" "src/cljs" "dev/cljs"]
                   :dependencies [[org.clojure/tools.namespace "0.2.11"]
                                  [com.cemerick/piggieback "0.2.2"]
                                  [figwheel-sidecar "0.5.16"]
                                  [day8.re-frame/re-frame-10x "0.3.3-react16"]]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}
             :provided {:dependencies [[org.clojure/clojurescript "1.10.238"]]}}

  :cljsbuild {:builds [{:id "dev"
                        :figwheel {:on-jsload "salesrelic.core/run"}
                        :source-paths ["src/cljs" "src/cljc" "dev/cljs"]
                        :compiler {:main "salesrelic.core"
                                   :optimizations :none
                                   :source-map true
                                   :source-map-timestamp true
                                   :asset-path "/js/app/out"
                                   :output-dir "resources/public/js/app/out"
                                   :output-to  "resources/public/js/app/app.js"
                                   :closure-defines      {"re_frame.trace.trace_enabled_QMARK_" true}
                                   :preloads             [day8.re-frame-10x.preload]
                                   }}
                       {:id "min"
                        :source-paths ["src/cljs" "src/cljc"]
                        :compiler {:main "salesrelic.core"
                                   :optimizations :simple
                                   :externs ["src/externs.js"]
                                   :asset-path "/js/app/out"
                                   :output-to  "resources/public/js/app/app.js"}}]}

  )
