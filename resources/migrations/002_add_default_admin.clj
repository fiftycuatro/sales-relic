(ns migrations.002-add-default-admin
  (:require
    [clojure.java.jdbc :as jdbc]
    [salesrelic.users :as users]
    [salesrelic.roles :as roles]))

(defn up
  [db-spec]
  (jdbc/with-db-transaction [tx db-spec]
    (let [ctx {:salesrelic/db tx}
          user-role  (roles/create<! ctx {:name :user})
          admin-role (roles/create<! ctx {:name :admin})
          user (users/create<! ctx {:name "admin" :password "password"})]
      (users/attach-role! ctx user admin-role))))

(defn down
  [db-spec]
  )
