(ns migrations.001-users
  (:require
      [salesrelic.database.core :as db]))

(defn up
  [db-spec]
  (db/install-uuid-extension db-spec)
  (db/create-updated-at-procedure db-spec)
  (db/create-users-table db-spec)
  (db/create-roles-table db-spec)
  (db/create-users-roles-table db-spec))

(defn down
  [db-spec]
  (db/drop-users-roles-table db-spec)
  (db/drop-roles-table db-spec)
  (db/drop-users-table db-spec)
  (db/drop-updated-at-procedure db-spec))
